
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "bitmap.h"

#include "shader_loader.h"

using namespace std;

shader *shader::current = NULL;

GLuint load_shader(GLuint kind, const char *file)
{
	GLint flen = 0;
	GLint res;
	GLchar *fbuf = NULL;
	GLuint num = 0;
	ifstream infile;
	
	if (kind == GL_VERTEX_SHADER)
		cerr << "Loading vertex shader " << file << endl;
	else
		cerr << "Loading fragment shader " << file << endl;
	infile.open(file);
	if (!infile)
	{
		cerr << "Couldn't open file!\n";
		exit(1);
	}
	infile.seekg(0, ios::end);
	flen = infile.tellg();
	infile.seekg(0, ios::beg);
	fbuf = new GLchar[flen+1];
	fbuf[flen] = '\0';
	infile.read(fbuf, flen);
	infile.close();
	
	num = glCreateShader(kind);
	
	glShaderSource(num, 1, (const GLchar**)&fbuf, NULL);
	delete [] fbuf;
	
	cerr << "Compiling... ";
	glCompileShader(num);
	
	glGetShaderiv(num, GL_COMPILE_STATUS, &res);
	if (res != GL_TRUE)
	{
		cerr << "failed!\n";
		glGetShaderiv(num, GL_INFO_LOG_LENGTH, &flen);
		
		fbuf = new GLchar[flen];
		glGetShaderInfoLog(num, flen, &res, fbuf);
		
		cerr << fbuf << endl;
		delete [] fbuf;
		exit(1);
	}
	else
		cerr << "successful.\n";
	
	return num;
}

shader::shader(const char *vfile, const char *pfile)
{
	GLchar *fbuf = NULL;
	GLint res, num;
	GLsizei length;
	GLint size;
	GLenum type;
	
	// load and compile shaders, report errors
	vs_num = load_shader(GL_VERTEX_SHADER, vfile);
	ps_num = load_shader(GL_FRAGMENT_SHADER, pfile);
	
	prog_num = glCreateProgram();
	glAttachShader(prog_num, vs_num);
	glAttachShader(prog_num, ps_num);
	
	// link program, report errors
	cerr << "Linking... ";
	glLinkProgram(prog_num);
	glGetProgramiv(prog_num, GL_LINK_STATUS, &res);
	if (res != GL_TRUE)
	{
		cerr << "failed!\n";
		glGetProgramiv(prog_num, GL_INFO_LOG_LENGTH, &res);
		
		fbuf = new GLchar[res];
		glGetProgramInfoLog(prog_num, res, &res, fbuf);
		
		cerr << fbuf << endl;
		delete [] fbuf;
		exit(1);
	}
	else
		cerr << "successful.\n";
	
	// report uniforms
	glGetProgramiv(prog_num, GL_ACTIVE_UNIFORMS, &num);
	glGetProgramiv(prog_num, GL_ACTIVE_UNIFORM_MAX_LENGTH, &res);
	fbuf = new GLchar[res+1];
	cerr << "Active uniforms:\n";
	for (int i = 0; i < num; i++)
	{
		glGetActiveUniform(prog_num, i, res, &length, &size, &type, fbuf);
		cerr << "\t" << i << ": " << fbuf << endl;
	}
	delete [] fbuf;
	
	// report attributes
	glGetProgramiv(prog_num, GL_ACTIVE_ATTRIBUTES, &num);
	glGetProgramiv(prog_num, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &res);
	fbuf = new GLchar[res+1];
	cerr << "Active attributes:\n";
	for (int i = 0; i < num; i++)
	{
		glGetActiveAttrib(prog_num, i, res, &length, &size, &type, fbuf);
		cerr << "\t" << i << ": " << fbuf << endl;
	}
	delete [] fbuf;
}

shader::~shader()
{
	glDetachShader(prog_num, vs_num);
	glDetachShader(prog_num, ps_num);
	glDeleteProgram(prog_num);
	glDeleteShader(vs_num);
	glDeleteShader(ps_num);
	
	if (tex_nums.size() > 0)
		glDeleteTextures(tex_nums.size(), &tex_nums[0]);
}

GLuint shader::get_uniform(const char *name)
{
	return glGetUniformLocation(prog_num, name);
}

GLuint shader::get_attribute(const char *name)
{
	return glGetAttribLocation(prog_num, name);
}

void shader::add_texture(const char *tfile)
{
	GLuint tnum;
	unsigned char *data = NULL;
	int width, height;
	
	cerr << "Adding texture " << tfile << endl;
	data = readBMP(tfile, width, height);
	
	if (!data)
	{
		cerr << "Couldn't open file!\n";
		exit(1);
	}
	
	glGenTextures(1, &tnum);
	glBindTexture(GL_TEXTURE_2D, tnum);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	
	delete [] data;
	
	tex_nums.push_back(tnum);
}

void shader::activate()
{
	glUseProgram(prog_num);
	current = this;
	
	// register textures
	char nbuf[] = "tex0";
	for (int i = 0; i < tex_nums.size() && i < 8; i++)
	{
		nbuf[3] = i + '0';
		glUniform1i(get_uniform(nbuf), i);
		
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, tex_nums[i]);
	}
}

void shader::deactivate()
{
	glUseProgram(0);
	current = NULL;
}

std::vector<shader*> shaders;
int shader_num = -1;

void next_shader()
{
	shader_num = (shader_num + 1) % shaders.size();
}
