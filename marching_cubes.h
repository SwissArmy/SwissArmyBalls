
#ifndef __MARCHING_CUBES
#define __MARCHING_CUBES

#include "glm/glm.hpp"

extern float (*march_fn)(const vec3 &p);
extern vec3 (*march_grad)(const vec3 &p);

void draw_march(const vec3 &mm, const vec3 &mx, int res, float thresh);

#endif
