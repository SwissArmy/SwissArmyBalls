
#ifndef __SHADER_LOADER
#define __SHADER_LOADER

// OpenGL library includes
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#elif defined(_WIN32)
#include <GL/glut.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <vector>

class shader
{
private:
	GLuint vs_num, ps_num, prog_num;
	std::vector<GLuint> tex_nums;
	
public:
	shader(const char *vfile, const char *pfile);
	~shader();
	
	GLuint get_uniform(const char *name);
	GLuint get_attribute(const char *name);
	
	void add_texture(const char *tfile);
	
	void activate();
	static void deactivate();
	
	static shader *current;
};

extern std::vector<shader*> shaders;
extern int shader_num;
void next_shader();

#endif
