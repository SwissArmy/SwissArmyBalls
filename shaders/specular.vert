// Example vertex shader

// Computes ambient term and diffuse term (in eye space)
// using the standard OpenGL lighting parameters

varying vec3 normal;

void main()
{
	vec4 ambient, globalAmbient, diffuse;
	vec3 lightDir;
	float NdotL;
	
	// compute the ambient and globalAmbient terms
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;
	
	// diffuse term
	normal = normalize(gl_NormalMatrix * gl_Normal);
	// directional light
	lightDir = normalize(vec3(gl_LightSource[0].position));
	NdotL = max(dot(normal, lightDir), 0.0);
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	
	// specular term // davilla
	
	// combine into lighting model
	gl_FrontColor = globalAmbient + ambient + NdotL * diffuse;
	
	// compute transformed vertex position
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
