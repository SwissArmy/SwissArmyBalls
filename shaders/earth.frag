// Example fragment shader

// Simple pass-through for color computed in vertex shader

varying vec3 normal;

varying vec4 diffuse;
varying vec4 vertexCoords;

uniform sampler2D tex0;
uniform sampler2D tex1;

void main()
{
	vec4 tex_color;
	float horizontalDist;
	
	// use position for texture coordinates
	horizontalDist = sqrt(vertexCoords.x * vertexCoords.x + vertexCoords.z * vertexCoords.z);
	gl_TexCoord[0].x = atan(vertexCoords.x, vertexCoords.z) / 3.1415926535898;
	gl_TexCoord[0].y = atan(horizontalDist, vertexCoords.y) / -3.1415926535898;
	
	// sample texture, use it as diffuse component
	tex_color = texture2D(tex0, gl_TexCoord[0].st);
	
	// combine results together into fragment color
	gl_FragColor = gl_Color + diffuse * tex_color;
	
	vec3 halfVector = normalize(gl_LightSource[0].halfVector.xyz);
	// specular term // davilla
	float NdotH = max(dot(normalize(normal), halfVector), 0.0);
	vec4 specular = texture2D(tex1, gl_TexCoord[0].st) * gl_LightSource[0].specular;
	
	// combine into lighting model
	gl_FragColor += pow(NdotH, gl_FrontMaterial.shininess) * specular; // davilla
}
