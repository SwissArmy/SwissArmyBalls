// Example vertex shader

// Computes ambient lighting term and planar texture coordinates.
// Passes texcoords and diffuse lighting multiplier to fragment shader.

varying vec4 vertexCoords;
varying vec3 normal;

void main()
{
	vec4 ambient, globalAmbient;
	
	// compute the ambient and globalAmbient terms
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;
	
	// diffuse term
	normal = normalize(gl_NormalMatrix * gl_Normal);
	
	// combine ambient terms
	gl_FrontColor = globalAmbient + ambient;
	
	// compute transformed vertex position
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	// use position for texture coordinates
	vertexCoords = gl_Vertex;
}
