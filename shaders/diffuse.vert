// Example vertex shader

// Computes ambient term and diffuse term (in eye space)
// using the standard OpenGL lighting parameters

void main()
{
	vec4 ambient, globalAmbient, diffuse;
	vec3 normal, lightDir;
	float NdotL;
	
	// compute the ambient and globalAmbient terms
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;
	
	// diffuse term
	normal = normalize(gl_NormalMatrix * gl_Normal);
	lightDir = normalize(vec3(gl_LightSource[0].position)); // directional light
	NdotL = max(dot(normal, lightDir), 0.0);
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	
	// combine into lighting model
	gl_FrontColor = globalAmbient + ambient + NdotL * diffuse;
	
	// compute transformed vertex position
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
	