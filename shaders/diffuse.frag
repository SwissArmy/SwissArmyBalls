// Example fragment shader

// Simple pass-through for color computed in vertex shader

void main()
{	
	gl_FragColor = gl_Color;
}