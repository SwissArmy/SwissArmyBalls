// Example fragment shader

// Simple pass-through for color computed in vertex shader

varying vec4 diffuse;
varying vec4 vertexCoords;
varying vec3 normal;

uniform sampler2D tex0;

void main()
{
	vec4 tex_color;
	
	normal = normalize(normal);
	
	vec3 r = 2.0 * dot(vertexCoords.xyz, normal) * normal - vertexCoords.xyz;
	
	float m = 2.0 * sqrt( (r.x * r.x) + (r.y * r.y) + pow(r.z + 1.0, 2.0));
	
	// use position for texture coordinates
	gl_TexCoord[0].s = r.x/m + 0.5;
	gl_TexCoord[0].t = r.y/m + 0.5;
	
	// sample texture, use it as diffuse component
	tex_color = texture2D(tex0, gl_TexCoord[0].st);
	
	// combine results together into fragment color
	gl_FragColor = gl_Color + diffuse * tex_color;
}
