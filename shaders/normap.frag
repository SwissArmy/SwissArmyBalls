// Example fragment shader

// Simple pass-through for color computed in vertex shader

varying vec4 vertexCoords;
varying vec3 normal;

uniform sampler2D tex0, tex1;

void main()
{
	vec4 tex_color, diffuse;
	vec3 lightDir, texNormal, rotatedNormal;
	float horizontalDist, theta, phi, alpha, beta, NdotL;
	
	// use position for texture coordinates
	horizontalDist = sqrt(vertexCoords.x * vertexCoords.x + vertexCoords.z * vertexCoords.z);
	gl_TexCoord[0].x = atan(vertexCoords.x, vertexCoords.z) / (0.25 * 3.1415926535898);
	gl_TexCoord[0].y = atan(horizontalDist, vertexCoords.y) / (-0.25 * 3.1415926535898);
	
	// bump mapping
	lightDir = normalize(vec3(gl_LightSource[0].position)); // directional light
	texNormal = normalize(2 * vec3(texture2D(tex1, gl_TexCoord[0].st)) - vec3(1.0, 1.0, 1.0));
	
	horizontalDist = sqrt(normal.x * normal.x + normal.z * normal.z);
	theta = atan(normal.x, normal.z);
	phi = atan(horizontalDist, normal.y);
	
	alpha = texNormal.y * cos(phi) - texNormal.z * sin(phi);
	beta = texNormal.y * sin(phi) - texNormal.z * cos(phi);
	rotatedNormal = vec3(beta * sin(theta) + texNormal.x * cos(theta), beta * cos(theta) - texNormal.x * sin(theta), -alpha);
	
	NdotL = max(dot(rotatedNormal, lightDir), 0.0);
	diffuse = NdotL * gl_LightSource[0].diffuse;
	
	// sample texture, use it as diffuse component
	tex_color = texture2D(tex0, gl_TexCoord[0].st);
	
	// combine results together into fragment color
	gl_FragColor = gl_Color + diffuse * tex_color;
}
