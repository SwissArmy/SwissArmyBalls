// Example fragment shader

// Simple pass-through for color computed in vertex shader

varying vec4 diffuse;

uniform sampler2D tex0;

void main()
{
	vec4 tex_color;
	float NdotL;
	
	// sample texture, use it as diffuse component
	tex_color = texture2D(tex0, gl_TexCoord[0].st);
	
	// combine results together into fragment color
	gl_FragColor = gl_Color + diffuse * tex_color;
}