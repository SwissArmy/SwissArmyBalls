// Example fragment shader

// Simple pass-through for color computed in vertex shader

varying vec3 normal;

void main()
{	
	gl_FragColor = gl_Color;
	
    vec3 halfVector = normalize(gl_LightSource[0].halfVector.xyz);
	// specular term // davilla
	float NdotH = max(dot(normalize(normal), halfVector), 0.0);
	vec4 specular = gl_FrontMaterial.specular * gl_LightSource[0].specular;
	
	// combine into lighting model
	gl_FragColor += pow(NdotH, gl_FrontMaterial.shininess) * specular; // davilla
}
