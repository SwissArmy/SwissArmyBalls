
#include <vector>

#include "glm/glm.hpp"
#include "glm/ext.hpp"

#include "particles.h"

using namespace std;
using namespace glm;

// particle storage
vector<vec3> particles, velocities;

void init_particles(int num)
{
	// particle initialization
	particles.resize(num);
	velocities.resize(num);
	
	// randomize initial positions and velocities
	for (int i = 0; i < particles.size(); i++)
	{
		particles[i] = 0.5f * vec3(signedRand1<float>(), signedRand1<float>(), signedRand1<float>());
		velocities[i] = 0.75f * vec3(signedRand1<float>(), signedRand1<float>(), signedRand1<float>());
	}
}

void update_particles(float dt)
{
	for (int i = 0; i < particles.size(); i++)
	{
		vec3 acc = -1.0f * particles[i];
		velocities[i] += dt * acc;
		particles[i] += dt * velocities[i];
	}
}

float meta_fn(const vec3 &p)
{
	float ret = 0.0f;
	vec3 d;
	
	for (int i = 0; i < particles.size(); i++)
	{
		d = p - particles[i];
		ret += 1.0f / length2(d);
	}
	
	return ret;
}

vec3 meta_grad(const vec3 &p)
{
	vec3 ret = vec3(0.0f);
	float tmp;
	vec3 d;
	
	for (int i = 0; i < particles.size(); i++)
	{
		d = p - particles[i];
		tmp = length2(d);
		ret += 2.0f * d / (tmp * tmp);
	}
	
	return normalize(ret);
}
