/*
 Marching cubes code for CS 354
 
 Parts adapted from http://paulbourke.net/geometry/polygonise/
 */

// C++ library includes
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>

// OpenGL library includes
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#elif defined(_WIN32)
#include <GL/glut.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

// GLM includes
#include "glm/glm.hpp"
#include "glm/ext.hpp"

using namespace std;
using namespace glm;

#include "marching_cubes.h"
#include "march_tables.inl"

struct tri
{
	glm::vec3 p[3];
};

struct gridcell
{
	glm::vec3 p[8];
	float val[8];
};

/*
 Linearly interpolate the position where an isosurface cuts
 an edge between two vertices, each with their own scalar value
 */
vec3 vertex_interp(float isolevel, vec3 p1, vec3 p2, float valp1, float valp2)
{
	float mu;
	vec3 p;
	
	if (fabs(isolevel - valp1) < 0.0001)
		return(p1);
	if (fabs(isolevel - valp2) < 0.0001)
		return(p2);
	if (fabs(valp1 - valp2) < 0.0001)
		return(p1);
	
	mu = (isolevel - valp1) / (valp2 - valp1);
	p = p1 + mu * (p2 - p1);
	
	return (p);
}

/*
 Given a grid cell and an isolevel, calculate the triangular
 facets required to represent the isosurface through the cell.
 Return the number of triangular facets, the array "tris"
 will be loaded up with the vertices at most 5 triangular facets.
 0 will be returned if the grid cell is either totally above
 of totally below the isolevel.
 */
int march_cell(gridcell grid, float isolevel, tri *tris)
{
	int i, ntriang;
	int cubeindex;
	vec3 vertlist[12];
	
	/*
	 Determine the index into the edge table which
	 tells us which vertices are inside of the surface
	 */
	cubeindex = 0;
	if (grid.val[0] < isolevel) cubeindex |= 1;
	if (grid.val[1] < isolevel) cubeindex |= 2;
	if (grid.val[2] < isolevel) cubeindex |= 4;
	if (grid.val[3] < isolevel) cubeindex |= 8;
	if (grid.val[4] < isolevel) cubeindex |= 16;
	if (grid.val[5] < isolevel) cubeindex |= 32;
	if (grid.val[6] < isolevel) cubeindex |= 64;
	if (grid.val[7] < isolevel) cubeindex |= 128;
	
	/* Cube is entirely in/out of the surface */
	if (edgeTable[cubeindex] == 0)
		return(0);
	
	/* Find the vertices where the surface intersects the cube */
	if (edgeTable[cubeindex] & 1)
		vertlist[0] =
		vertex_interp(isolevel,grid.p[0],grid.p[1],grid.val[0],grid.val[1]);
	if (edgeTable[cubeindex] & 2)
		vertlist[1] =
		vertex_interp(isolevel,grid.p[1],grid.p[2],grid.val[1],grid.val[2]);
	if (edgeTable[cubeindex] & 4)
		vertlist[2] =
		vertex_interp(isolevel,grid.p[2],grid.p[3],grid.val[2],grid.val[3]);
	if (edgeTable[cubeindex] & 8)
		vertlist[3] =
		vertex_interp(isolevel,grid.p[3],grid.p[0],grid.val[3],grid.val[0]);
	if (edgeTable[cubeindex] & 16)
		vertlist[4] =
		vertex_interp(isolevel,grid.p[4],grid.p[5],grid.val[4],grid.val[5]);
	if (edgeTable[cubeindex] & 32)
		vertlist[5] =
		vertex_interp(isolevel,grid.p[5],grid.p[6],grid.val[5],grid.val[6]);
	if (edgeTable[cubeindex] & 64)
		vertlist[6] =
		vertex_interp(isolevel,grid.p[6],grid.p[7],grid.val[6],grid.val[7]);
	if (edgeTable[cubeindex] & 128)
		vertlist[7] =
		vertex_interp(isolevel,grid.p[7],grid.p[4],grid.val[7],grid.val[4]);
	if (edgeTable[cubeindex] & 256)
		vertlist[8] =
		vertex_interp(isolevel,grid.p[0],grid.p[4],grid.val[0],grid.val[4]);
	if (edgeTable[cubeindex] & 512)
		vertlist[9] =
		vertex_interp(isolevel,grid.p[1],grid.p[5],grid.val[1],grid.val[5]);
	if (edgeTable[cubeindex] & 1024)
		vertlist[10] =
		vertex_interp(isolevel,grid.p[2],grid.p[6],grid.val[2],grid.val[6]);
	if (edgeTable[cubeindex] & 2048)
		vertlist[11] =
		vertex_interp(isolevel,grid.p[3],grid.p[7],grid.val[3],grid.val[7]);
	
	/* Create the tri */
	ntriang = 0;
	for (i=0; triTable[cubeindex][i]!=-1; i+=3) {
		tris[ntriang].p[0] = vertlist[triTable[cubeindex][i  ]];
		tris[ntriang].p[1] = vertlist[triTable[cubeindex][i+1]];
		tris[ntriang].p[2] = vertlist[triTable[cubeindex][i+2]];
		ntriang++;
	}
	
	return (ntriang);
}

float (*march_fn)(const vec3 &p) = NULL;
vec3 (*march_grad)(const vec3 &p) = NULL;

vector<float> vcache;
vector<vec3> pcache;

#define MARCH_IDX(i, j, k) ((i) * (res) * (res) + (j) * (res) + (k))

void draw_march(const vec3 &mm, const vec3 &mx, int res, float thresh)
{
	int i, j, k, idx, ntris;
	tri tris[5]; // single cell triangle storage
	vec3 dm = (mx - mm) / (float)(res);
	vec3 v;
	gridcell g;
	
	res += 1;
	
	vcache.resize((res) * (res) * (res));
	pcache.resize(vcache.size());
	
	// first pass, cache values
	v = mm;
	for (i = 0; i < res; i++)
	{
		for (j = 0; j < res; j++)
		{
			for (k = 0; k < res; k++)
			{
				idx = MARCH_IDX(i, j, k);
				pcache[idx] = v;
				vcache[idx] = march_fn(v);
				
				v.z += dm.z;
			}
			v.y += dm.y;
			v.z = mm.z;
		}
		v.x += dm.x;
		v.y = mm.y;
		v.z = mm.z;
	}
	
	// second pass, extract triangles and render
	glBegin(GL_TRIANGLES);
	for (i = 0; i < res-1; i++)
	{
		for (j = 0; j < res-1; j++)
		{
			for (k = 0; k < res-1; k++)
			{
				idx = MARCH_IDX(i, j, k);
				g.p[0] = pcache[idx];
				g.val[0] = vcache[idx];
				idx = MARCH_IDX(i+1, j, k);
				g.p[1] = pcache[idx];
				g.val[1] = vcache[idx];
				idx = MARCH_IDX(i+1, j, k+1);
				g.p[2] = pcache[idx];
				g.val[2] = vcache[idx];
				idx = MARCH_IDX(i, j, k+1);
				g.p[3] = pcache[idx];
				g.val[3] = vcache[idx];
				idx = MARCH_IDX(i, j+1, k);
				g.p[4] = pcache[idx];
				g.val[4] = vcache[idx];
				idx = MARCH_IDX(i+1, j+1, k);
				g.p[5] = pcache[idx];
				g.val[5] = vcache[idx];
				idx = MARCH_IDX(i+1, j+1, k+1);
				g.p[6] = pcache[idx];
				g.val[6] = vcache[idx];
				idx = MARCH_IDX(i, j+1, k+1);
				g.p[7] = pcache[idx];
				g.val[7] = vcache[idx];
				
				ntris = march_cell(g, thresh, tris);
				
				for (int q = 0; q < ntris; q++)
				{
					v = march_grad(tris[q].p[0]);
					glNormal3fv(value_ptr(v));
					glVertex3fv(value_ptr(tris[q].p[0]));
					
					v = march_grad(tris[q].p[1]);
					glNormal3fv(value_ptr(v));
					glVertex3fv(value_ptr(tris[q].p[1]));
					
					v = march_grad(tris[q].p[2]);
					glNormal3fv(value_ptr(v));
					glVertex3fv(value_ptr(tris[q].p[2]));
				}
			}
		}
	}
	glEnd();
}

