
#ifndef __PARTICLES
#define __PARTICLES

#include "glm/glm.hpp"

// particle stuff
void init_particles(int num);
void update_particles(float dt);

// marching cubes functions for metaballs
float meta_fn(const glm::vec3 &p);
glm::vec3 meta_grad(const glm::vec3 &p);

#endif
