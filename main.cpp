/*
 CS 354 - Fall 2011
 main.cpp

 Skeleton code for Project 4
 */

// C++ library includes
#include <cstdlib>
#include <cmath>
#include <vector>
#include <iostream>

// OpenGL library includes
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#elif defined(_WIN32)
#include <GL/glut.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

// GLM includes
#include "glm/glm.hpp"
#include "glm/ext.hpp"

using namespace std;
using namespace glm;

// includes from this project
#include "marching_cubes.h"
#include "shader_loader.h"
#include "particles.h"

const float globbiness = 12.0;

// window parameters
int window_width = 800, window_height = 600;
float window_aspect = (float)window_width / (float)window_height;

// camera parameters
#define rad(X) ((X) * 1.745329251994e-02)
vec3 poi = vec3(), eye = vec3();
float theta = 0, phi = 0, r = 3;
mat4 cam_transform;
bool paused = false;

enum cmode { CAM_NONE, CAM_ORBIT, CAM_PAN, CAM_ZOOM };
cmode cam_mode = CAM_NONE;

// shadow parameters for mouse motion
float theta_click = 0, phi_click = 0, r_click = 3;
vec3 poi_click = vec3();

// mouse parameters
float mx, my;
float mx_click, my_click;

// callback prototypes
void initGL();
void display();
void resize(int width, int height);
void mouseButton(int button, int state, int x, int y);
void mouseMotion(int x, int y);
void keyboard(unsigned char key, int x, int y);

void initGL()
{
	// load and register shaders
	shader *sh = NULL;
	
	// TODO: register your shaders and textures here
	sh = new shader("shaders/diffuse.vert", "shaders/diffuse.frag");
	shaders.push_back(sh);
	
    sh = new shader("shaders/specular.vert", "shaders/specular.frag");
	shaders.push_back(sh);
	
	sh = new shader("shaders/planar.vert", "shaders/planar.frag");
	sh->add_texture("textures/rock.bmp");
	shaders.push_back(sh);
	
	sh = new shader("shaders/cylandrical.vert", "shaders/cylandrical.frag");
	sh->add_texture("textures/earth.bmp");
	shaders.push_back(sh);
	
	sh = new shader("shaders/spherical.vert", "shaders/spherical.frag");
	sh->add_texture("textures/earth.bmp");
	shaders.push_back(sh);
	
	sh = new shader("shaders/earth.vert", "shaders/earth.frag");
	sh->add_texture("textures/earth.bmp");
	sh->add_texture("textures/earth_spec.bmp");
	shaders.push_back(sh);
	
	sh = new shader("shaders/envmap.vert", "shaders/envmap.frag");
	sh->add_texture("textures/sphere_map.bmp");
	shaders.push_back(sh);
	
	sh = new shader("shaders/normap.vert", "shaders/normap.frag");
	sh->add_texture("textures/rock.bmp");
	sh->add_texture("textures/rock_norm.bmp");
	shaders.push_back(sh);
	
	// load up first shader
	next_shader();

    // enable depth testing, and set clear color to white
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0, 1.0, 1.0, 1.0);
	
    // resize the window
    resize(window_width, window_height);
	
	mouseMotion(0, 0); // force camera to initialize
	
	// setup particles
	init_particles(4);
	
	// register metaball rendering functions
	march_fn = meta_fn;
	march_grad = meta_grad;
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear last frame

    // load the identity matrix (we're in modelview mode)
    glLoadIdentity();
	glMultMatrixf(value_ptr(cam_transform)); // multiply in the camera transform
	
	// set up material parameters
	float diff[4] = { 0, 0, 1, 1 };
	float spec[4] = { 1, 1, 1, 1 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50.0f);
	
	// draw isosurface with marching cubes
	shaders[shader_num]->activate();
	float radius = 1.0f;
	draw_march(radius * vec3(-1, -1, -1), radius * vec3(1, 1, 1), 30, globbiness);
	
	// update particle positions
	if (!paused)
		update_particles(1.0f / 120.0f);

	// draw a bounding box around it
	shader::deactivate();
	glDisable(GL_LIGHTING);
	glColor3f(0, 0, 0);
	glutWireCube(2.0f * radius);

    glFlush(); // finish the drawing commands
    glutSwapBuffers(); // and update the screen
	glutPostRedisplay();
}

void resize(int width, int height)
{
    // This reshape function is called whenever the user
    // resizes the display window.

    window_width = width;
    window_height = height;
	window_aspect = (float)width / (float)height;

    // resize the window
    glViewport(0, 0, window_width, window_height);

    // setup basic orthographic projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluPerspective(45.0, window_aspect, 0.5, 100.0);

    // switch over to modelview
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glutPostRedisplay(); // let glut know to redraw the screen
}

void mouseButton(int button, int state, int x, int y)
{
    /*
     This function is called whenever the user presses
     or releases a mouse button.

     The button variable can be tested equal to GLUT_LEFT_BUTTON,
     GLUT_MIDDLE_BUTTON, and GLUT_RIGHT_BUTTON, which are self-
     explanatory.

     The state variable is either GLUT_UP or GLUT_DOWN, also self-
     explanatory.

     The x and y variables are the mouse's location in window coordinates.
     */

	y = window_height - y;
	
	mx = mx_click = (float)x / (float)window_width;
	my = my_click = (float)y / (float)window_height;
	
	if (state == GLUT_DOWN)
	{
		if (button == GLUT_LEFT_BUTTON)
			cam_mode = CAM_ORBIT;
		else if (button == GLUT_MIDDLE_BUTTON)
			cam_mode = CAM_PAN;
		else // if (button == GLUT_RIGHT_BUTTON)
			cam_mode = CAM_ZOOM;
		
		poi_click = poi;
		theta_click = theta;
		phi_click = phi;
		r_click = r;
	}
	else // if state == GLUT_UP
		cam_mode = CAM_NONE;

    glutPostRedisplay(); // let glut know to redraw the screen
}

void mouseMotion(int x, int y)
{
    /*
     This function is called whenever the user moves the mouse with
     a button down.  x and y represent the mouse's new location.
     */

	y = window_height - y;
	
	mx = (float)x / (float)window_width;
	my = (float)y / (float)window_height;
	
	switch (cam_mode)
	{
		case CAM_ORBIT:
			theta = theta_click + -10.0f * (mx - mx_click);
			phi = phi_click + -10.0f * (my - my_click);
			phi = (phi < rad(88.0f)) ? phi : rad(88.0f);
			phi = (phi > rad(-88.0f)) ? phi : rad(-88.0f);
			break;
		case CAM_PAN:
		{
			vec3 sx = vec3(cam_transform[0].x, cam_transform[1].x, cam_transform[2].x);
			vec3 sy = vec3(cam_transform[0].y, cam_transform[1].y, cam_transform[2].y);
			poi = poi_click + -20.0f * (mx - mx_click) * sx + -20.0f * (my - my_click) * sy;
		}
			break;
		case CAM_ZOOM:
			r = r_click + -50.0f * (my - my_click);
			r = (r > 0.2f) ? r : 0.2f;
			break;
	}
	
	eye.x = r * cos(phi) * sin(theta);
	eye.y = r * sin(phi);
	eye.z = r * cos(phi) * cos(theta);
	eye += poi;
	
	cam_transform = lookAt(eye, poi, vec3(0, 1, 0));

    glutPostRedisplay(); // let glut know to redraw the screen
}

void keyboard(unsigned char key, int x, int y)
{
    /*
     This function is called whenever the user hits letters or numbers
     on the keyboard.  The 'key' variable has the character the user hit,
     and x and y tell where the mouse was when it was hit.
     */

	y = window_height - y;

    // YOUR CODE HERE
	switch (key)
	{
		case ' ':
			paused = !paused;
			break;
		case 'f':
			next_shader();
			break;
		case 'q':
		case 27: // esc
			exit(0);
			break;
	}

    glutPostRedisplay(); // let glut know to redraw the screen
}

int main(int argc, char *argv[])
{
    // Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("CS 354: Marching cubes");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutMouseFunc(mouseButton);
    glutMotionFunc(mouseMotion);
    glutKeyboardFunc(keyboard);
	
#if !(defined(__APPLE__) || defined(MACOSX))
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		exit(1);
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
#endif

    initGL();

    // Set glut running
    glutMainLoop();

    return 0;
}
